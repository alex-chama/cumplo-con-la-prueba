# Cumplo -con la- Prueba

----
## ¿En qué consiste?
La señorita Valentina decidió aceptar ambas pruebas para encontrar su lugar en esta interesante empresa.

Por lo tanto, enlistaré a modo de tener a mano las pruebas recibidas.

### Desafío Back-end
_En Cumplo operamos créditos. Desafortunadamente a veces, algunos créditos no son pagados en plazo y por tanto, caen en mora. Para calcular los intereses de esa mora, nos regimos por la TMC (tasa máxima convencional)._

_El desafío es que construyas una aplicación que a partir de las las características de un crédito (monto del crédito y plazo), nos muestre la TMC que corresponde. Dado que la TMC cambia día a día queremos poder consultar por la TMC de cualquier día en particular_

_Puedes ocupar la API Key que desees, pero te entregamos la siguiente por si quieres ocuparla_

**API Key: **9c84db4d447c80c74961a72245371245cb7ac15f

_**El sistema debe responder realizando lo siguiente:**_

_

- _El usuario de la aplicación debe poder especificar el monto en UF, el plazo en días y la fecha en la que quieres saber la TMC_
- _Según esos parámetros la aplicación debe entregar la TMC correspondiente al día consultado_
**Consideramos entregable: **

- _Acceso al código fuente_
- _Acceso a la aplicación funcional_
**Requerimientos tecnológicos:**

- _Python con framework Django_
_

### Desafío Front-end
_Construir una aplicación web que permita obtener y visualizar el valor del dólar para un período de tiempo determinado, consultando a la API de la SBIF. La aplicación debe contar con lo siguiente:_
  
Aspectos funcionales:  
  
- Definir el rango de tiempo a consultar (fecha inicial y fecha final).  
- Invocar la API de la SBIF para obtener los valores  del dólar.  
- Mostrar y visualizar en un gráfico los valores consultados para el rango de fechas. Este punto es importante, juega con el gráfico como gustes para el usuario.   
- Mostrar promedio, valor máximo y mínimo en el rango de fechas definido. __Este punto es importante, juega con el gráfico como gustes para el usuario.__
  
Aspectos no funcionales:  
  
- Usar convenciones de Web Semántica en el HTML.  
- Optimizar el uso de CSS para la presentación de estilos.  
- Utilizar patrones de diseño en programación en el código Javascript usado.  
- Actualizar de forma asíncrona el contenido y los gráficos asociados al modificar el rango de fechas  
  
Consideramos entregable:
- _Acceso al código fuente_
- _Acceso a la aplicación funcional_
_  
Requerimientos tecnológicos:  
  
- HTML  
- React  
  
**API Key: **9c84db4d447c80c74961a72245371245cb7ac15f

**Documentación de la API**

[http://api.sbif.cl/documentacion/index.html](http://api.sbif.cl/documentacion/index.html)

## Resumen de los requerimientos

### Requerimientos Funcionales

- Obtener el TMC del dia según las UF y plazo del crédito.
- Visualizar el precio del dolar entre rangos de fecha mediante gráficos y demases.

### Requerimientos no funcionales
- Utilizar Python API + Django
- Utilizar Django + React

## Lista de Tareas

- [x] Ententender los API de TMC y dolar creando sus funciones asociadas para obtener los datos necesarios.
- [x] Levantar droplet.
- [ ] Iniciar proyecto gitlab
- [ ] Iniciar proyecto Django
- [ ] Conectar Django con APIs
- [ ] Iniciar React
- [ ] Conectar React con API de Django
--
- [ ] Solo si alcanzo, escalar proyecto a una base de datos que almacene los valores anuales anteriores y todos los dias a las 4 de la mañana, se recuperen los datos del dia y resto del año.
